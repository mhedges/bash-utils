#!/usr/bin/env bash
#
# This is a rather minimal example Argbash potential
# Example taken from http://argbash.readthedocs.io/en/stable/example.html
#
# ARG_OPTIONAL_SINGLE([config], [c], [custom path to rubocop.yml], )
# ARG_OPTIONAL_BOOLEAN([verbose], [v], [Turn on verbose mode], )
# ARG_OPTIONAL_BOOLEAN([parallel], [p], [Turn on verbose mode], on)
# ARG_OPTIONAL_BOOLEAN([auto-correct], [a], [Auto-correct offenses (only when it's safe).], )
# ARG_OPTIONAL_BOOLEAN([disable-uncorrectable], , [Used with --auto-correct to annotate any offenses that do not support autocorrect], )
# ARG_HELP([Run rubocop w/ the ePublishing guarantee. Provides a handy wrapper for doing normal rubocop things.])
# ARGBASH_GO

# [ <-- needed because of Argbash

# echo "Value of --rubocop: $_arg_rubocop"
# echo "Value of --config: $_arg_config"
# echo "Value of --verbose: $_arg_verbose"
# echo "Value of --parallel: $_arg_parallel"
# echo "Value of --auto-correct: $_arg_auto_correct"
# echo "Value of --disable-uncorrectable: $_arg_disable_uncorrectable"
# Find Rubcop

RUBOCOP=$(rbenv which rubocop)
$RUBOCOP -c /home/mhedges/rubocop.yml --parallel

# ] <-- needed because of Argbash
