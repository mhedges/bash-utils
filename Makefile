PREFIX ?= /usr/local
cwd := $(shell pwd)
bindir := bin
srcdir := src
srcfiles := $(foreach d, $(srcdir), $(wildcard $(d)/*))
srcfiles := $(foreach f, $(srcfiles), $(basename $(f)))
srcfiles := $(foreach f, $(srcfiles), $(notdir $(f)))
binfiles := $(foreach d, $(bindir), $(wildcard $(d)/*))

build:
	$(foreach f, $(srcfiles), argbash $(srcdir)/$(f).m4 -o $(bindir)/$(f))

install:
	$(foreach f, $(binfiles), install $(f) $(PREFIX)/$(f);)

uninstall:
	$(foreach f, $(binfiles), rm -f $(PREFIX)/$(f))